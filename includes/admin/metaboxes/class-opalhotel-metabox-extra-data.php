<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class OpalHotel_MetaBox_Extra_Data {

	/* render */
	public static function render( $post ) {
		$extra = OpalHotel_Extra::instance( $post );
		/* filter */
		$tab_panels = apply_filters( 'opalhotel_extra_data_tab', array(
				'general' => array(
					'label'  => __( 'General', 'opal-hotel-room-booking' ),
					'target' 	=> 'general_extra_data',
					'icon'		=> 'fa fa-cog'
				) 
		) );

		?>
		<div id="opalhotel_extra_data_container" class="opalhotel_metabox_data_container">
		<?php wp_nonce_field( 'opalhotel_save_data', 'opalhotel_meta_nonce' ); ?>
			<ul class="opalhotel_metabox_data_tabs">
				<?php $i = 0; foreach ( $tab_panels as $key => $tab ) : ?>

					<li class="opalhotel_tab <?php echo esc_attr( $key ) ?>">
						<a href="#<?php echo esc_attr( $tab['target'] ) ?>" class="<?php echo $i === 0 ? ' active' : '' ?>">
							<?php if ( isset( $tab['icon'] ) ) : ?>
								<i class="<?php echo esc_attr( $tab['icon'] ) ?>"></i>
							<?php endif; ?>
							<?php echo esc_html( $tab['label'] ); ?>
						</a>
					</li>

				<?php $i++; endforeach; ?>
			</ul>

			<?php foreach ( $tab_panels as $key => $tab ) : ?>

				<?php do_action( 'opalhotel_extra_data_before_tab_panel', $key, $tab ); ?>

				<?php if ( $key === 'general' ) : ?>

					<div id="general_extra_data" class="opalhotel_room_data_panel active">
  
						<!-- charge amount -->
						<div class="opalhotel_field_group">
							<?php   print_r( $name ); ?>
							<label for="discount_amount"><?php _e( 'Amount', 'opal-hotel-room-booking' ); ?></label>
							<input type="number" step="any" min="0" name="post_content" id="discount_amount" value="<?php echo esc_attr( floatval( $extra->post_content ) ) ?>" />
							<span class="opalhotel_tiptip" data-tip="<?php esc_attr_e( 'Set charge value extra.', 'opal-hotel-room-booking' ); ?>"><i class="fa fa-question-circle-o"></i></span>
							<?php do_action( 'opalhotel_room_data_extra_amount' ); ?>
						</div> 
					</div> 
					
				<?php else : ?>

					<?php do_action( 'opalhotel_extra_data_tab_panel', $key, $tab ); ?>

				<?php endif; ?>

				<?php do_action( 'opalhotel_extra_data_after_tab_panel', $key, $tab ); ?>

			<?php endforeach; ?>
		</div>

		<?php
	}

	/* save post meta*/
	public static function save( $post_id, $post ) {
		if ( $post->post_type !== 'opalhotel_extra' || empty( $_POST ) ) {
			return;
		}

		/* each save meta has prefix _extra */
		foreach ( $_POST as $name => $value ) { 
			if ( strpos( $name, '_extra' ) === 0 ) {
				
				update_post_meta( $post_id, sanitize_key( $name ), $value );
				do_action( 'opalhotel_extra_update_post_meta', $name, $value, $post_id );
			}
		}

	}

}