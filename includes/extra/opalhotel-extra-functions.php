<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

if ( ! function_exists( 'opalhotel_extra_discount_type_label' ) ) {

	function opalhotel_extra_discount_type_label() {
		return apply_filters( 'opalhotel_extra_discount_type_label', array(
				'fixed_cart'	=> __( 'Extra Discount', 'opal-hotel-room-booking' ),
				'percent_cart'	=> __( 'Extra % Discount', 'opal-hotel-room-booking' )
			) );
	}

}

if ( ! function_exists( 'opalhotel_extra_useaged' ) ) {
	/* return extra used time */
	function opalhotel_extra_useaged( $coupon_id = null ) {
		return;
	}
}

if ( ! function_exists( 'opalhotel_get_extra_by_code' ) ) {

	/* get extra object by code same as post title */
	function opalhotel_get_extra_by_code( $code = '', $price = 0 ) {
		$code = sanitize_title( $code );

		global $wpdb;
    	$extra = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type = %s AND post_status = %s", $code, 'opalhotel_extra', 'publish' ) );

    	if ( ! $extra ) {
    		return new WP_Error( 'extra_not_found', sprintf( __( 'Extra code %s not found.', 'opal-hotel-room-booking' ), $code ) );
    	}

    	$extra = OpalHotel_Extra::instance( $extra )->is_available( $price );

    	if ( is_wp_error( $extra ) ) {
    		return $extra;
    	}

    	return $extra;

	}

}