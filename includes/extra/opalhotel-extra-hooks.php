<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

add_filter( 'manage_opalhotel_extra_posts_columns', 'opalhotel_extra_manage_posts_columns', 99 );
if ( ! function_exists( 'opalhotel_extra_manage_posts_columns' ) ) {

	// manage add room post type columns
	function opalhotel_extra_manage_posts_columns( $columns ) {
		
		unset( $columns['title'] );
		unset( $columns['author'] );
		unset( $columns['comments'] ); 
		$add_columns = array(
				'cb'				=> '<input type="checkbox" />',
				'code'				=> __( 'Code', 'opal-hotel-room-booking' ),
				'content'			=> __( 'Amount', 'opal-hotel-room-booking' ) 
			);

		return array_merge( $add_columns, $columns );
	}
}
add_action( 'manage_opalhotel_extra_posts_custom_column', 'opalhotel_extra_manage_posts_custom_columns', 1 );
if ( ! function_exists( 'opalhotel_extra_manage_posts_custom_columns' ) ) {

	/* room custom columns */
	function opalhotel_extra_manage_posts_custom_columns( $column ) {
		global $post; $extra = OpalHotel_Extra::instance( $post->ID );
		
		switch ( $column ) {
			case 'code':
				echo '<a href="' . get_edit_post_link( $post->ID ) . '">' . get_the_title( $post->ID ) . '</a>';
				break;
				
			case 'content': 
				printf( __( $extra->post_content.' (%s)', 'opal-hotel-room-booking' ), opalhotel_get_currency_symbol() ); 
			break;

			case 'type':
				$labels = opalhotel_extra_discount_type_label();
				echo isset( $labels[ $extra->extra_type ] ) ? esc_attr( $labels[ $extra->extra_type ] ) : '';
				break;

			case 'usage_limit':
				echo absint( $extra->extra_usaged_time );
				if ( $extra->extra_usage_time_limit ) {
					echo ' / ' . absint( $extra->extra_usage_time_limit );
				} else {
					echo ' / ' . __( 'No limit', 'opal-hotel-room-booking' );
				}
				break;

			case 'expire':
				if ( $extra->extra_expire_timestamp ) {
					echo opalhotel_format_date( $extra->extra_expire_timestamp );
				} else {
					echo __( 'No Expiry', 'opal-hotel-room-booking' );
				}
				break;
				
			
		}
	}
}

add_action( 'opalhotel_update_status_completed', 'opalhotel_update_extra_usaged_time' );
if ( ! function_exists( 'opalhotel_update_extra_usaged_time' ) ) {
	function opalhotel_update_extra_usaged_time( $order_id ) {
		$order = OpalHotel_Order::instance( $order_id );
		if ( $order->extra && isset( $order->extra['id'] ) && $order->extra['id'] ) {
			$extra = OpalHotel_Extra::instance( $order->extra['id'] );
			$usage_time = absint( $extra->extra_usaged_time );
			update_post_meta( $order->extra['id'], '_extra_usaged_time', $usage_time + 1 );
		}
	}
}