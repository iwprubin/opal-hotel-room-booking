<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

class OpalHotel_Extra {

	/* protected id of post type */
	public $id = null;

	// instance insteadof new class
	static $instance = null;


	/* public data */
	public $data = null;

	/* constructor */
	public function __construct( $id ) {
		/* set data */
		if ( is_numeric( $id ) && $post = get_post( $id ) ) {
			$this->id = $post->ID;
			$this->data = $post;
		} else if ( $id instanceof WP_Post ) {
			$this->id = $id->ID;
			$this->data = $id;
		}
	}

	/* set magic method */
	public function __set( $key, $value = null ) {
		$this->data->{$key} = $value;
	}

	/* get magic method */
	public function __get( $key = null ) {
		if ( ! $key ) return;
		return $this->get( $key );
	}

	/* get data */
	public function get( $name = null, $default = null ) {

		if ( isset( $this->data->{$name} ) ) {
			/* get data post*/
			return $this->data->{$name};
		} else if ( metadata_exists( 'post', $this->id, '_' . $name ) ) {
			/* get post meta */
			return get_post_meta( $this->id, '_' . $name, true );
		} else if ( method_exists( $this, $name ) ) {
			/* get method */
			return $this->{$name}();
		}

		/* return default */
		return $default;
	}

	/* check available */
	public function is_available( $price = null ) {

		if ( ! $this->id ) {
			return new WP_Error( 'extra_not_exists', sprintf( __( 'Extra code <span>%s</span> is not exists.', 'opal-hotel-room-booking' ), $this->id ) );
		}

		if ( ! $price ) {
			$price = 0;
		}

		/* validate expire time */
		$current_time = time();
		if ( $current_time >= $this->extra_expire_timestamp ) {
			return new WP_Error( 'extra_expired', sprintf( __( 'Extra code <span>%s</span> has been expired.', 'opal-hotel-room-booking' ), $this->id ) );
		}

		/* vaildate useage time */
		if ( $this->extra_usage_time_limit && opalhotel_extra_useaged( $this->id ) >= $this->extra_usage_time_limit ) {
			return new WP_Error( 'extra_usage_limited', sprintf( __( 'Extra code <span>%s</span> has been limited.', 'opal-hotel-room-booking' ), $this->id ) );
		}

		/* minium spend */
		if ( $this->extra_minimum_spend && $this->extra_minimum_spend >= $price ) {
			return new WP_Error( 'extra_spend', sprintf( __( 'Your total must elder <span>%s</span>.', 'opal-hotel-room-booking' ), opalhotel_format_price( $this->extra_minimum_spend ) ) );
		}

		/* discount amount */
		if ( $this->extra_type === 'fixed_cart' && $this->extra_amount > $price ) {
			return new WP_Error( 'extra_spend', sprintf( __( 'Your total must elder <span>%s</span>.', 'opal-hotel-room-booking' ), opalhotel_format_price( $this->extra_amount ) ) );
		}

		/* minium spend */
		if ( $this->extra_maximum_spend && $this->extra_maximum_spend <= $price ) {
			return new WP_Error( 'extra_spend', sprintf( __( 'Your total must lesser <span>%s</span>.', 'opal-hotel-room-booking' ), opalhotel_format_price( $this->extra_maximum_spend ) ) );
		}

		return $this->id;

	}

	/* discount */
	public function calculate_discount( $subtotal = 0 ) {
		if ( ! $subtotal ) {
			return;
		}

		$discount = $this->extra_amount;
		if ( $this->extra_type === 'percent_cart' ) {
			$discount = $subtotal * floatval( $this->extra_amount ) / 100;
		}

		return apply_filters( 'opalhotel_extra_discount', $discount, $this );
	}

	/**
	 * instance insteadof new class
	 * @param  $extra optional Eg: id, object
	 * @return object
	 */
	static function instance( $extra = null ) {
		$id = null;
		if ( $extra instanceof WP_POST ) {
			$id = $extra->ID;
		} else if ( is_numeric( $extra ) ) {
			$id = $extra;
		} else if ( is_object( $extra ) && isset( $extra->ID ) ) {
			$id = $extra->ID;
		}

		if ( empty( self::$instance[ $id ] ) ) {
			self::$instance[ $id ] = new self( $id );
		}

		return self::$instance[ $id ];

	}

}
