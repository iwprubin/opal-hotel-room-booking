<?php
/**
 * The template for displaying room content within loops
 *
 * This template can be overridden by copying it to yourtheme/opalhotel/checkout/customer-info.php.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

$user = opalhotel_get_current_customer();


// You need styling for the datepicker. For simplicity I've linked to Google's hosted jQuery UI CSS. 
// wp_register_style( 'jquery-ui', OPALHOTEL_URI . 'assets/site/css/jquery-ui.css'  );
// wp_enqueue_style( 'jquery-ui' );  

 
?>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.opalhotel_customer_flight_pickup input { 
    position: relative;
    top: -3px;
    vertical-align: middle;
    width: 13px;
}
.opalhotel_customer_flight_dropoff input { 
    position: relative;
    top: -3px;
    vertical-align: middle;
    width: 13px;
}
.ui-datepicker {
	min-width: 305px;
}
</style>
<h3 class="opalhotel-section-title hide"><?php _e( 'Payment Details', 'opal-hotel-room-booking' ); ?></h3>

<div class="opalhotel_reservation_customer_group left">

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_first_name">
			<?php esc_html_e( 'First Name', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="text" name="opalhotel_customer_first_name" id="opalhotel_customer_first_name" value="<?php echo esc_attr( $user->first_name ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_email">
			<?php esc_html_e( 'Email', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="email" name="opalhotel_customer_email" id="opalhotel_customer_email" value="<?php echo esc_attr( $user->email ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_city">
			<?php esc_html_e( 'City', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="text" name="opalhotel_customer_city" id="opalhotel_customer_city" value="<?php echo esc_attr( $user->city ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_country">
			<?php esc_html_e( 'Country', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<?php echo opalhotel_dropdown_country( array( 'name' => 'opalhotel_customer_country', 'selected' => $user->country ) ) ?>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_address">
			<?php esc_html_e( 'Address', 'opal-hotel-room-booking' ); ?>
		</label>
		<textarea name="opalhotel_customer_address" id="opalhotel_customer_address" placeholder="<?php esc_attr_e( 'Enter your address.', 'opal-hotel-room-booking' ); ?>" rows="5"><?php echo esc_html( $user->address ); ?></textarea>
	</div>

	<div class="opalhotel-form-group">
		<div class="opalhotel_customer_flight_pickup">
			<label for="opalhotel_customer_flight_pickup">
				<input name="opalhotel_customer_flight_pickup" id="opalhotel_customer_flight_pickup" type="checkbox" value="yes"> Request Flight Pickup
			</label>
		</div> 
	</div> 
	
	<div class="hidden-flight-pickup-block" style="display:none;"> 
		
		<div class="opalhotel-form-group">
			<label for="opalhotel_customer_airline">
				<?php esc_html_e( 'Airline', 'opal-hotel-room-booking' ); ?>
			</label>
			<input type="text" name="opalhotel_customer_airline" id="opalhotel_customer_airline" />
		</div> 

		<div class="opalhotel-form-group">
			<label for="opalhotel_customer_pax">
				<?php esc_html_e( 'No. Pax pickup', 'opal-hotel-room-booking' ); ?>
			</label>
			<input type="text" name="opalhotel_customer_pax" id="opalhotel_customer_pax" />
		</div> 

		
		<div class="opalhotel-form-group">
			<label for="opalhotel_customer_arrival_time">
				<?php esc_html_e( 'Arrival Time', 'opal-hotel-room-booking' ); ?>
			</label>
			<input type="text" name="opalhotel_customer_arrival_time" id="opalhotel_customer_arrival_time" />
		</div> 
	</div> 

	<div class="hidden-flight-dropoff-block" style="display:none;">  

		<div class="opalhotel-form-group">
			<label for="opalhotel_customer_dropoff">
				<?php esc_html_e( 'Drop Off Remarks', 'opal-hotel-room-booking' ); ?>
			</label>
			<textarea name="opalhotel_customer_dropoff" id="opalhotel_customer_dropoff" placeholder="<?php esc_attr_e( 'Notes.', 'opal-hotel-room-booking' ); ?>" rows="5"> </textarea>
		</div> 
	</div> 

</div>

<div class="opalhotel_reservation_customer_group right">

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_last_name">
			<?php esc_html_e( 'Last Name', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="text" name="opalhotel_customer_last_name" id="opalhotel_customer_last_name" value="<?php echo esc_attr( $user->last_name ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_phone">
			<?php esc_html_e( 'Phone', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="text" name="opalhotel_customer_phone" id="opalhotel_customer_phone" value="<?php echo esc_attr( $user->phone ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_state">
			<?php esc_html_e( 'State', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="text" name="opalhotel_customer_state" id="opalhotel_customer_state"  value="<?php echo esc_attr( $user->state ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_postcode">
			<?php esc_html_e( 'Postcode', 'opal-hotel-room-booking' ); ?>
			<span class="required">*</span>
		</label>
		<input type="text" name="opalhotel_customer_postcode" id="opalhotel_customer_postcode" value="<?php echo esc_attr( $user->postcode ) ?>"/>
	</div>

	<div class="opalhotel-form-group">
		<label for="opalhotel_customer_notes">
			<?php esc_html_e( 'Addtional Notes', 'opal-hotel-room-booking' ); ?>
		</label>
		<textarea name="opalhotel_customer_notes" id="opalhotel_customer_notes" placeholder="<?php esc_attr_e( 'Notes.', 'opal-hotel-room-booking' ); ?>" rows="5"><?php echo esc_html( $user->notes ); ?></textarea>
	</div> 
		

	<div class="opalhotel-form-group">
		<div class="opalhotel_customer_flight_dropoff">
			<label for="opalhotel_customer_flight_dropoff">
				<input name="opalhotel_customer_flight_dropoff" id="opalhotel_customer_flight_dropoff" type="checkbox" value="yes"> Request Flight Drop-off
			</label>
		</div> 
	</div> 

	<div class="hidden-flight-pickup-block" style="display:none;">
		
		<div class="opalhotel-form-group">
			<label for="opalhotel_customer_flight_no">
				<?php esc_html_e( 'Flight No', 'opal-hotel-room-booking' ); ?>
			</label>
			<input type="text" name="opalhotel_customer_flight_no" id="opalhotel_customer_flight_no" />
		</div> 

		<div class="opalhotel-form-group">
			<label for="opalhotel_customer_arrival_date">
				<?php esc_html_e( 'Arrival Date', 'opal-hotel-room-booking' ); ?>
			</label>
			<input type="text" name="opalhotel_customer_arrival_date" id="opalhotel_customer_arrival_date"  readonly /> 
		</div>  
	</div>  
</div>
<script type="text/javascript"> 
jQuery('input#opalhotel_customer_flight_pickup').change(function() {
	if(this.checked) {
		jQuery(".hidden-flight-pickup-block").show();
	} else {
	  jQuery(".hidden-flight-pickup-block").hide();
	}
});
jQuery('input#opalhotel_customer_flight_dropoff').change(function() {
	if(this.checked) {
		jQuery(".hidden-flight-dropoff-block").show();
	} else {
	  jQuery(".hidden-flight-dropoff-block").hide();
	}
}); 

jQuery(document).ready(function($) {
	jQuery("#opalhotel_customer_arrival_date").datepicker();
	 
});
 

</script>
