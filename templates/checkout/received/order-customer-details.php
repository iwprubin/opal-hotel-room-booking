<?php
/**
 * The template for displaying room content within loops
 *
 * This template can be overridden by copying it to yourtheme/opalhotel/checkout/received/order-customer-details.php.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

?>

<?php do_action( 'opalhotel_before_order_customer_details', $order ); ?>

<div class="opalhotel_order_customer_details">
	
	<h3><?php _e( 'Customer Details', 'opal-hotel-room-booking' ); ?></h3>

	
	<div class="row">
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'Name', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_name"><?php printf( '%s', $order->get_customer_name() ) ?></span>
		</div>
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'Email', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_email"><?php printf( '%s', $order->customer_email ) ?></span>
		</div>
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'Phone', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_phone"><?php printf( '%s', $order->customer_phone ) ?></span>
		</div>
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'Address', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_address"><?php printf( '%s', $order->customer_address ) ?></span>
		</div>
	</div>
	
	<div class="row">
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'State', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_state"><?php printf( '%s', $order->customer_state ) ?></span>
		</div>
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'City', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_city"><?php printf( '%s', $order->customer_city ) ?></span>
		</div>
		<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<label><?php _e( 'Country', 'opal-hotel-room-booking' ); ?></label>
			<span class="customer_country"><?php printf( '%s', $order->customer_country ) ?></span>
		</div>
	</div>

	<p><?php printf( '%s', $order->get_customer_notes() ) ?></p>
	
	<?php 
	if( $order->customer_flight_pickup ){
		?> 
		<hr /> 
		<h3><?php _e( 'Flight Pickup Details', 'opal-hotel-room-booking' ); ?></h3>
		<div class="row">
			<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<label><?php _e( 'Airline', 'opal-hotel-room-booking' ); ?></label>
				<span class="customer_airline"><?php printf( '%s', $order->customer_airline ) ?></span>
			</div>
			<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<label><?php _e( 'Flight No.', 'opal-hotel-room-booking' ); ?></label>
				<span class="customer_flight_no"><?php printf( '%s', $order->customer_flight_no ) ?></span>
			</div>
			<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<label><?php _e( 'No. Pax', 'opal-hotel-room-booking' ); ?></label>
				<span class="customer_pax"><?php printf( '%s', $order->customer_pax ) ?></span>
			</div>
			<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<label><?php _e( 'Arrival Date', 'opal-hotel-room-booking' ); ?></label>
				<span class="customer_arrival_date"><?php printf( '%s', $order->customer_arrival_date ) ?></span>
			</div>
		</div>
		<div class="row">
			<div class="customer col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<label><?php _e( 'Arrival Time', 'opal-hotel-room-booking' ); ?></label>
				<span class="customer_arrival_time"><?php printf( '%s', $order->customer_arrival_time ) ?></span>
			</div> 
		</div>
		<div class="row">
			<p class="customer col-xs-12"><em>Note: The flight pick-up is still pending for approval, and it will be added on on your current booking total upon your arrival.</em></p>
		</div> 
		<?php
	} 
	
	if( $order->customer_flight_dropoff ){
		?> 
		<hr /> 
		<h3><?php _e( 'Flight Drop Off Details', 'opal-hotel-room-booking' ); ?></h3>
		<div class="row">
			<div class="customer col-sm-6 col-xs-12">
				<label><?php _e( 'Remarks', 'opal-hotel-room-booking' ); ?></label>
				<span class="customer_dropoff"><?php printf( '%s', $order->customer_dropoff ) ?></span>
			</div> 
		</div> 
		<div class="row">
			<p class="customer col-xs-12"><em>Note: The flight dropf-off is still pending for approval, and it will be added on on your booking payment. It details to be discussed @ check-in</em></p>
		</div> 
		<?php
	}
	?>
</div>

<?php do_action( 'opalhotel_after_order_customer_details', $order ); ?>