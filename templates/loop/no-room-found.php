<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

?>

<h1><?php _e( 'No rooms found in our system.', 'opal-hotel-room-booking' ); ?></h1>